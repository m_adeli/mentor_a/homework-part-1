package com.adeli;

import com.sun.deploy.security.SelectableSecurityManager;

import java.util.Scanner;

public class Sum {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        System.out.println("Enter number 1 (max length is 50): ");
        sb1.append(scan.next());
        System.out.println("Enter number 2 (max length is 50): ");
        sb2.append(scan.next());
        int max = Math.max(sb1.length(), sb2.length());
        int diff = Math.abs(sb1.length() - sb2.length());
        if (sb1.length() > sb2.length())
            for (int i = 0; i < diff; i++)
                sb2.insert(0, "0");
        else
            for (int i = 0; i < diff; i++)
                sb1.insert(0, "0");

        char[] number1 = sb1.reverse().substring(0).toCharArray();
        char[] number2 = sb2.reverse().substring(0).toCharArray();

        StringBuilder sb3 = new StringBuilder();

        int sum;
        int div = 0;
        for (int i = 0; i < max; i++) {
            int a = Integer.parseInt(String.valueOf(number1[i]));
            int b = Integer.parseInt(String.valueOf(number2[i]));
            sum = div + a + b;
            if (sum > 9) {
                sb3.append(sum % 10);
                div = 1;
            } else {
                sb3.append(sum);
                div = 0;
            }
        }
        if (div != 0)
            sb3.append(div);
        System.out.println(sb3.reverse());
    }
}
