package com.adeli;

import java.util.Scanner;

public class PythagorasNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a: ");
        int a = scan.nextInt();
        System.out.println("Enter b: ");
        int b = scan.nextInt();
        System.out.println("Enter c: ");
        int c = scan.nextInt();
        System.out.println(((a + b) > c) && ((a + c) > b) && ((b + c) > a));
    }
}
