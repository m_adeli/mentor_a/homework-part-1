package com.adeli;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.*;
import java.util.stream.IntStream;

public class RepeatedNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a stream of numbers: ");
        String input = scan.next();
        int[] numbers = new int[10];
        for (char c : input.toCharArray()) {
            int i = Integer.parseInt(String.valueOf(c));
            numbers[i]++;
        }
        for (int i = 0; i < 10; i++)
            System.out.println(i + " " + numbers[i]);
    }
}
