package com.adeli;

import java.util.Scanner;

public class Fibomax {

    public static void main(String[] args) {
        int n1 = 0;
        int n2 = 1;
        int n3;
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int limit = scan.nextInt();

        //loop starts from 2 because 0 and 1 are already printed
        for (int i = 0; i < limit; ++i)
        {
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
            if (n3 >= limit)
                break;
        }
        System.out.println(n1);
    }
}
