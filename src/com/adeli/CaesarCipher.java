package com.adeli;

import java.util.*;

public class CaesarCipher {

    public static void main(String[] args) {
        String sb1 = "abcdefghijklmnopqrstuvwxyz";
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter decryption left shift: ");
        int count = scan.nextInt();
        String sb2 = sb1.substring(count, 26) + sb1.substring(0, count);
        HashMap<Character, Character> convertMap = new HashMap<>();
        for (int i = 0; i < 26; i++) {
            convertMap.put(sb1.charAt(i), sb2.charAt(i));
        }
        System.out.print("Enter your word to cipher: ");
        String plainText = scan.next().toLowerCase();
        String cipherText = "";
        for (char c : plainText.toCharArray()) {
            cipherText += String.valueOf(convertMap.get(c));
        }
        System.out.println("Cipher text of your word: " + cipherText);
    }
}
