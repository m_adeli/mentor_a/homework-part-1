package com.adeli;

import java.util.HashMap;
import java.util.Scanner;

public class ReverseKeyPad {
    public static void main(String[] args) {
        HashMap<Character, String> keys = new HashMap<>();
        keys.put('.', "1");
        keys.put('!', "11");
        keys.put('?', "111");
        keys.put('a', "2");
        keys.put('b', "22");
        keys.put('c', "222");
        keys.put('d', "3");
        keys.put('e', "33");
        keys.put('f', "333");
        keys.put('g', "4");
        keys.put('h', "44");
        keys.put('i', "444");
        keys.put('j', "5");
        keys.put('k', "55");
        keys.put('l', "555");
        keys.put('m', "6");
        keys.put('n', "66");
        keys.put('o', "666");
        keys.put('p', "7");
        keys.put('q', "77");
        keys.put('r', "777");
        keys.put('s', "8");
        keys.put('t', "88");
        keys.put('u', "888");
        keys.put('v', "9");
        keys.put('w', "99");
        keys.put('x', "999");
        keys.put('y', "0");
        keys.put('z', "00");
        keys.put(' ', "000");

        Scanner scan = new Scanner(System.in);
        System.out.print("Enter your words: ");
        String input = scan.nextLine().toLowerCase();
        String result = "";
        for (char c : input.toCharArray()) {
            result = result + " " + keys.get(c);
        }

        System.out.println(result);

    }
}
