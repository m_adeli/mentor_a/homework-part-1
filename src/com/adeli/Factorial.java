package com.adeli;

public class Factorial {
    public static void main(String[] args) {
        System.out.println(Fact(5L));
    }

    public static Long Fact(Long n) {
        if (n == 0)
            return 1L;
        else
            return Fact(n - 1) * n;
    }
}
