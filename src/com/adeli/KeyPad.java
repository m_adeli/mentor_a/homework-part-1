package com.adeli;

import java.util.HashMap;
import java.util.Scanner;

public class KeyPad {
    public static void main(String[] args) {
        HashMap<String, String> keys = new HashMap<>();
        keys.put("1", ".");
        keys.put("11", "!");
        keys.put("111", "?");
        keys.put("2", "a");
        keys.put("22", "b");
        keys.put("222", "c");
        keys.put("3", "d");
        keys.put("33", "e");
        keys.put("333", "f");
        keys.put("4", "g");
        keys.put("44", "h");
        keys.put("444", "i");
        keys.put("5", "j");
        keys.put("55", "k");
        keys.put("555", "l");
        keys.put("6", "m");
        keys.put("66", "n");
        keys.put("666", "o");
        keys.put("7", "p");
        keys.put("77", "q");
        keys.put("777", "r");
        keys.put("8", "s");
        keys.put("88", "t");
        keys.put("888", "u");
        keys.put("9", "v");
        keys.put("99", "w");
        keys.put("999", "x");
        keys.put("0", "y");
        keys.put("00", "z");
        keys.put("000", " ");

        Scanner scan = new Scanner(System.in);
        System.out.print("Enter your keys: ");
        String input = scan.nextLine();
        String result = "";
        String[] numbers = input.split(" ");
        for (String str : numbers) {
            result = result + keys.get(str);
        }

        System.out.println(result);
    }
}
