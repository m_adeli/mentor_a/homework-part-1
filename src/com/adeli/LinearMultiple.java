package com.adeli;

import java.util.Scanner;

public class LinearMultiple {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter numbers: ");
        String input = scan.nextLine();
        String output = "";
        while (input.length() > 1) {
            output = "";
            if (input.length() % 2 != 0)
                input += "1";
            char[] numbers = input.toCharArray();
            for (int i = 0; i < numbers.length; i++) {
                int a = Integer.parseInt(String.valueOf(numbers[i++]));
                int b = Integer.parseInt(String.valueOf(numbers[i]));
                output += String.valueOf(a * b);
            }
            input = output;
        }
        System.out.println(output);
    }
}
